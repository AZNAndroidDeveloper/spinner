package uz.azn.lesson12

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView


class CustomAdapter(context: Context, customList: ArrayList<CustomItems?>) :
    ArrayAdapter<CustomItems?>(context, 0, customList) {
    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        return customView(position, convertView, parent)!!
    }

    override fun getDropDownView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        return customView(position, convertView, parent)!!
    }

    fun customView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View? {
        var convertView = convertView
        if (convertView == null) {
            convertView =
                LayoutInflater.from(context).inflate(R.layout.spinner_layout, parent, false)
        }
        val items = getItem(position)
        val myTxtView = convertView!!.findViewById<TextView>(R.id.tv_spinner)
        if (items != null) {
            myTxtView.text = items.name
        }
        return convertView
    }
}