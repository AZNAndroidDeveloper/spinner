package uz.azn.lesson12

import android.graphics.Color

enum class Colors(val colors: Int) {
    Red(colors = Color.RED),
    Green(colors = Color.GREEN),
    Blue(colors = Color.BLUE),
    Dark(colors = Color.DKGRAY),
    Yellow(colors = Color.YELLOW)
}