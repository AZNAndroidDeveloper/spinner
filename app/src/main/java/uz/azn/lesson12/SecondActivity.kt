package uz.azn.lesson12

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Adapter
import android.widget.AdapterView
import uz.azn.lesson12.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() ,AdapterView.OnItemSelectedListener{
    var spinnerText = ""
    val binding:ActivitySecondBinding by lazy { ActivitySecondBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


        val colorList:MutableList<CustomItems?> = ArrayList()
        colorList.add(CustomItems(Colors.Red.toString()))
        colorList.add(CustomItems(Colors.Blue.toString()))
        colorList.add(CustomItems(Colors.Green.toString()))
        colorList.add(CustomItems(Colors.Yellow.toString()))
        colorList.add(CustomItems(Colors.Dark.toString()))

        val adapter  = CustomAdapter(this,colorList as ArrayList)

        binding.spinner.adapter = adapter
        binding.spinner.onItemSelectedListener = this

        binding.saveButton.setOnClickListener {
            Log.d("MainActivity", spinnerText.toString())
        val intent =  Intent(applicationContext,MainActivity::class.java)
            intent.putExtra("color",spinnerText.toString())
            val preference = getSharedPreferences("boolean", Context.MODE_PRIVATE)
            val editor:SharedPreferences.Editor = preference.edit()
            editor.putBoolean("isLoggid",true)
          editor.putString("color",spinnerText.toString())
            editor.apply()
            startActivity(intent)
            finish()
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        val item  = parent!!.selectedItem as CustomItems
        spinnerText = item.name
        binding.textView.text  =spinnerText

    }
}