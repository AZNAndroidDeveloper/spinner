package uz.azn.lesson12

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import uz.azn.lesson12.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        val sharedPreferences= getSharedPreferences("boolean",Context.MODE_PRIVATE)
        val isHave= sharedPreferences.getBoolean("isLoggid", false)
        Log.d("User", isHave.toString())
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val color = sharedPreferences.getString("color", "")
        binding.changeColorBtn.setOnClickListener {
            startActivity(Intent(applicationContext, SecondActivity::class.java))
            finish()
        }
        val colorList: MutableList<CustomItems?> = ArrayList()
        colorList.add(CustomItems(Colors.Red.toString()))
        colorList.add(CustomItems(Colors.Blue.toString()))
        colorList.add(CustomItems(Colors.Green.toString()))
        colorList.add(CustomItems(Colors.Yellow.toString()))
        colorList.add(CustomItems(Colors.Dark.toString()))
        val colors: MutableList<Int> = ArrayList()
        colors.add(-0x10000)
        colors.add(-0xffff01)
        colors.add(-0xff0100)
        colors.add(-0x100)
        colors.add(-0xbbbbbc)


        for (i in colorList.indices) {
            if (colorList[i]?.name == color) {
                binding.layoutOne.setBackgroundColor(colors[i])
                break

            }
        }
    }
}